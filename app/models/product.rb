class Product < ActiveRecord::Base
  attr_accessible :description, :manufacturer, :name, :picture_url, :price, :website_url
  validates :name, :manufacturer, :picture_url, :website_url, length: { maximum: 200 }
  validates :name, presence: true, length: { minimum: 1 }
  validates :description, length: { maximum: 500 }
  validates :price, numericality: true, allow_nil: true
end
