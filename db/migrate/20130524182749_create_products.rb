class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.float :price
      t.string :manufacturer
      t.string :website_url
      t.string :picture_url

      t.timestamps
    end
  end
end
