# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
one = Product.create(
  name: "Hp Laserjet P2015 A4",
  description: "Print speed, black (best quality mode) Up to 27 ppm",
  price: 90,
  manufacturer: "Hewlett-Packard",
  website_url: "http://h10010.www1.hp.com/wwpc/us/en/sm/WF06b/18972-236251-236263-14638-f51-1845551-1845552-1845554.html?dnr=1",
  picture_url: "assets/P2015.jpg"
)

two = Product.create(
  name: "Hp Laserjet M3035 xs MFP",
  description: "Accomplish multiple office tasks: printing, copying, scanning, and faxing with this reliable, easy-to-use HP LaserJet MFP. Enhance productivity with embedded send-to-e-mail/network folder functionality, and expandable options.
                Ideal for small workteams of up to 8 users in enterprise environments, this very reliable, black-and-white HP LaserJet MFP enables them to do handle multiple office workflows from a single, easy-to-use device.",
  price: 2899,
  manufacturer: "Hewlett-Packard",
  website_url: "http://www8.hp.com/ca/en/products/printers/product-detail.html?oid=3435697",
  picture_url: "assets/m3035.jpg"
)