require 'test_helper'

class ProductTest < ActiveSupport::TestCase

   setup do
      @productOne = products(:one)
   end
   
   test "shouldn't save without name" do
      product = @productOne
      product.name = nil
      assert !product.save, "saved without a name"
   end

   test "shouldn't save with too long name" do
      product = @productOne
      product.name = "a" * 201
      assert !product.save, "saved with a name length of 201"
   end

   test "shouldn't save with too long description" do
      product = @productOne
      product.description = "a" * 501
      assert !product.save, "saved with a description length of 501"
   end

   test "shouldn't save price with other characters than numbers" do
      product = @productOne
      product.price = "a"
      assert !product.save, "saved price with other characters than numbers"
   end

   test "shouldn't save picture_url longer than 200" do
      product = @productOne
      product.picture_url = "a"*201
      assert !product.save, "saved picture_url with length of 201"
   end

   test "shouldn't save website_url longer than 200" do
      product = @productOne
      product.website_url = "a"*201
      assert !product.save, "saved website_url with length of 201"
   end

   test "shouldn't save manufacturer longer than 200" do
      product = @productOne
      product.manufacturer = "a"*201
      assert !product.save, "saved manufacturer with length of 201"
   end


end
