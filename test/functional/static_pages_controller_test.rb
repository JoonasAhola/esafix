require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "should get home page" do
    get :home
    assert_response :success, "didn't get home page with GET :home"
  end
end
